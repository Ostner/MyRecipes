//
//  View+Extensions.swift
//  MyRecipes
//
//  Created by Tobias Ostner on 4/2/17.
//  Copyright © 2017 Tobias Ostner. All rights reserved.
//

import UIKit

extension UIView {
    func constraintToEdgesOfParent() {
        guard let parent = superview else { return }
        NSLayoutConstraint.activate(
          [
            self.bottomAnchor.constraint(equalTo: parent.bottomAnchor),
            self.leadingAnchor.constraint(equalTo: parent.leadingAnchor),
            self.topAnchor.constraint(equalTo: parent.topAnchor),
            self.trailingAnchor.constraint(equalTo: parent.trailingAnchor)
          ])
    }
}
