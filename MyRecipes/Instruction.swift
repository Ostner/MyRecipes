//
//  Instruction.swift
//  MyRecipes
//
//  Created by Tobias Ostner on 4/11/17.
//  Copyright © 2017 Tobias Ostner. All rights reserved.
//

import Foundation

struct Instruction {
    let step: Int
    let title: String
}

extension Instruction {
    var json: [String:Any] {
        return ["step": step, "title": title]
    }

    var jsonData: Data {
        return try! JSONSerialization.data(withJSONObject: json, options: [])
    }
}
