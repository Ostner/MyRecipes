//
//  Recipe.swift
//  MyRecipes
//
//  Created by Tobias Ostner on 4/2/17.
//  Copyright © 2017 Tobias Ostner. All rights reserved.
//

import Foundation

struct Recipe {
    let name: String

    let ingredients: [Ingredient]
    let instructions: [Instruction]

    init(name: String,
         ingredients: [Ingredient] = [],
         instructions: [Instruction] = []) {
        self.name = name
        self.ingredients = ingredients
        self.instructions = instructions
    }

    init?(json: [String: Any]) {
        guard let name = json["name"] as? String else { return nil }
        self.name = name
        self.ingredients = []
        self.instructions = []
    }

    var json: [String:Any] {
        return ["name": name]
    }

    var jsonData: Data {
        return try! JSONSerialization.data(withJSONObject: json, options: [])
    }
}
