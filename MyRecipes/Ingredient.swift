//
//  Ingredient.swift
//  MyRecipes
//
//  Created by Tobias Ostner on 4/9/17.
//  Copyright © 2017 Tobias Ostner. All rights reserved.
//

import Foundation

struct Ingredient {
    let title: String

    var json: [String:Any] {
        return ["title": title]
    }

    var jsonData: Data {
        return try! JSONSerialization.data(withJSONObject: json, options: [])
    }
}
