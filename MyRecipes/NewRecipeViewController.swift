//
//  AddRecipeViewController.swift
//  MyRecipes
//
//  Created by Tobias Ostner on 4/1/17.
//  Copyright © 2017 Tobias Ostner. All rights reserved.
//

import UIKit

final class NewRecipeViewController: UITableViewController {

    let cellIdentifier = "Cell"

    let headerTitles = ["Name", "Ingredients", "Instructions"]

    var name = "" {
        didSet {
            navigationItem.rightBarButtonItem?.isEnabled = !name.isEmpty
        }
    }

    var ingredients: [String] = []
    var instructions: [Instruction] = []

    override init(style: UITableViewStyle) {
        super.init(style: style)
        let submit = UIBarButtonItem(
          barButtonSystemItem: .save,
          target: self,
          action: #selector(submit(_:)))
        submit.isEnabled = false
        navigationItem.rightBarButtonItem = submit
        let cancelBtn = UIBarButtonItem(
          barButtonSystemItem: .cancel,
          target: self,
          action: #selector(cancel))
        navigationItem.leftBarButtonItem = cancelBtn
        tableView.register(
          NewRecipeCell.self,
          forCellReuseIdentifier: cellIdentifier
        )
        tableView.isEditing = true
    }

    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    // MARK: UITableViewDataSource

    override func numberOfSections(in: UITableView) -> Int {
        return 3
    }

    override func tableView(
      _ tableView: UITableView,
      numberOfRowsInSection section: Int)
      -> Int
    {
        switch section {
        case 0: return 1
        case 1: return ingredients.count
        case 2: return instructions.count
        default: return 0
        }
    }

    override func tableView(
      _ tableView: UITableView,
      cellForRowAt indexPath: IndexPath)
      -> UITableViewCell
    {
        let cell = tableView.dequeueReusableCell(
          withIdentifier: cellIdentifier,
          for: indexPath
        ) as! NewRecipeCell
        cell.delegate = self
        cell.enableDeletion = indexPath.section > 0
        switch indexPath.section {
        case 0: cell.textField.text = name
        case 1: cell.textField.text = ingredients[indexPath.row]
        case 2: cell.textField.text = instructions[indexPath.row].title
        default: break
        }
        return cell
    }

    override func tableView(
      _ tableView: UITableView,
      titleForHeaderInSection section: Int)
      -> String?
    {
        return headerTitles[section]
    }

    // MARK: UITableViewDelegate

    override func tableView(
      _ tableView: UITableView,
      editingStyleForRowAt indexPath: IndexPath)
      -> UITableViewCellEditingStyle
    {
        return .none
    }

    override func tableView(
      _ tableView: UITableView,
      shouldIndentWhileEditingRowAt indexPath: IndexPath)
      -> Bool
    {
        return false
    }

    override func tableView(
      _ tableView: UITableView,
      heightForFooterInSection section: Int)
      -> CGFloat
    {
        return section > 0 ? 50 : 0
    }

    override func tableView(
      _ tableView: UITableView,
      viewForFooterInSection section: Int)
      -> UIView?
    {
        switch section {
        case 1,2:
            let btn = UIButton(type: .custom)
            btn.setTitle("Add", for: .normal)
            let action = section == 1 ?
              #selector(addEmptyIngredient) :
              #selector(addEmptyInstruction)
            btn.addTarget(
              self,
              action: action,
              for: .touchUpInside
            )
            btn.backgroundColor = .green
            return btn
        default:
            return nil
        }
    }

    override func tableView(
      _ tableView: UITableView,
      moveRowAt oldIndexPath: IndexPath,
      to newIndexPath: IndexPath)
    {
        switch oldIndexPath.section {
        case 1:
            let ingredient = ingredients[oldIndexPath.row]
            ingredients.remove(at: oldIndexPath.row)
            ingredients.insert(ingredient, at: newIndexPath.row)
        case 2:
            let instruction = Instruction(
              step: newIndexPath.row + 1,
              title: instructions[oldIndexPath.row].title
            )
            instructions.remove(at: oldIndexPath.row)
            instructions.insert(instruction, at: newIndexPath.row)
        default: break
        }
        tableView.reloadData()
    }

    override func tableView(
      _ tableView: UITableView,
      canMoveRowAt indexPath: IndexPath)
      -> Bool
    {
        switch indexPath.section {
        case 0: return false
        case 1: return ingredients.count > 1
        case 2: return instructions.count > 1
        default: return false
        }
    }

    override func tableView(
      _ tableView: UITableView,
      targetIndexPathForMoveFromRowAt oldIndexPath: IndexPath,
      toProposedIndexPath newIndexPath: IndexPath)
      -> IndexPath
    {
        switch (oldIndexPath.section, newIndexPath.section) {
        case let (old, new) where old > new:
            return IndexPath(row: 0, section: oldIndexPath.section)
        case let (old, new) where old < new:
            let count = old == 1 ? ingredients.count : instructions.count
            return IndexPath(row: count - 1, section: oldIndexPath.section)
        default:
            return newIndexPath
        }
    }

    // MARK: Actions

    func submit(_ sender: UIButton) {
        print("submit")
        let ingreds = ingredients
          .filter { !$0.isEmpty }
          .map(Ingredient.init)
        let recipe = Recipe(name: name,
                            ingredients: ingreds,
                            instructions: instructions)
        WebClient().post(recipe) {
            result in
            print(result ?? "Couldn't post recipe")
            DispatchQueue.main.async {
                _ = self.navigationController?.popViewController(animated: true)
            }
        }
    }

    func cancel() {
        _ = navigationController?.popViewController(animated: true)
    }

    func addEmptyIngredient(_ sender: UIButton) {
        resignAllResponders()
        ingredients.append("")
        tableView.reloadSections([1], with: .automatic)
    }

    func addEmptyInstruction() {
        resignAllResponders()
        instructions.append(
          Instruction(step: instructions.count+1, title: "")
        )
        tableView.reloadSections([2], with: .automatic)
    }

    // MARK: Private

    private func resignAllResponders() {
        guard let indexPaths = tableView.indexPathsForVisibleRows else { return }
        for indexPath in indexPaths {
            let cell = tableView.cellForRow(at: indexPath)! as! NewRecipeCell
            cell.textField.endEditing(true)
        }
    }

}

extension NewRecipeViewController: NewRecipeCellDelegate {
    func newRecipeCell(_ cell: NewRecipeCell, didCommitValue value: String) {
        guard let indexPath = tableView.indexPath(for: cell) else { return }
        switch indexPath.section {
        case 0: name = value
        case 1: ingredients[indexPath.row] = value
        case 2:
            let instruction = Instruction(
              step: indexPath.row + 1,
              title: value
            )
            instructions[indexPath.row] = instruction
        default: break
        }
    }

    func didDeleteNewRecipeCell(_ cell: NewRecipeCell) {
        guard let indexPath = tableView.indexPath(for: cell) else { return }
        switch indexPath.section {
        case 1: ingredients.remove(at: indexPath.row)
        case 2: instructions.remove(at: indexPath.row)
        default: break
        }
        tableView.reloadSections([indexPath.section], with: .automatic)
    }
}
