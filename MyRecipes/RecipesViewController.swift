//
//  ViewController.swift
//  MyRecipes
//
//  Created by Tobias Ostner on 3/29/17.
//  Copyright © 2017 Tobias Ostner. All rights reserved.
//

import UIKit

final class RecipesViewController: UIViewController, UICollectionViewDataSource, UICollectionViewDelegate {

    var collectionView: UICollectionView!
    var layout: UICollectionViewFlowLayout!
    var addRecipeBtn: UIButton!
    var cellIdentifier = "Cell"
    var recipes: [Recipe] = []

    let addRecipeBtnWidth: CGFloat = 70

    override func viewDidLoad() {
        super.viewDidLoad()
        configureCollectionView()
        configureAddButton()
        let syncBtn = UIBarButtonItem(
          barButtonSystemItem: .refresh,
          target: self,
          action: #selector(refresh)
        )
        navigationItem.rightBarButtonItems = [syncBtn]
    }

    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        layout.itemSize = CGSize(width: collectionView.bounds.width, height: 100)
    }

    // MARK: UICollectionViewDataSource

    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection: Int) -> Int {
        return recipes.count
    }

    func collectionView(
      _ collectionView: UICollectionView,
      cellForItemAt indexPath: IndexPath)
      -> UICollectionViewCell
    {
        let cell = collectionView.dequeueReusableCell(
          withReuseIdentifier: cellIdentifier,
          for: indexPath
        ) as! RecipeCell
        cell.backgroundColor = .purple
        cell.title.text = recipes[indexPath.item].name
        return cell
    }

    // MARK: UICollectionViewDelegate

    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let vc = RecipeDetailViewController(recipe: recipes[indexPath.item])
        show(vc, sender: self)
    }

    // MARK: Actions

    func newRecipe() {
        let newRecipeVC = NewRecipeViewController(style: .grouped)
        show(newRecipeVC, sender: addRecipeBtn)
    }

    func refresh() {
        WebClient().getRecipes {
            recipes in
            DispatchQueue.main.async {
                self.recipes = recipes ?? []
                self.collectionView.reloadData()
            }
        }
    }

    // MARK: Private

    private func configureCollectionView() {
        layout = UICollectionViewFlowLayout()
        collectionView = UICollectionView(frame: .zero, collectionViewLayout: layout)
        collectionView.backgroundColor = .red
        collectionView.translatesAutoresizingMaskIntoConstraints = false
        view.addSubview(collectionView)
        collectionView.constraintToEdgesOfParent()
        collectionView.register(
          RecipeCell.self,
          forCellWithReuseIdentifier: cellIdentifier
        )
        collectionView.dataSource = self
        collectionView.delegate = self
    }

    private func configureAddButton() {
        addRecipeBtn = UIButton(type: .system)
        addRecipeBtn.backgroundColor = .green
        addRecipeBtn.layer.cornerRadius = addRecipeBtnWidth / 2
        addRecipeBtn.addTarget(
          self,
          action: #selector(newRecipe),
          for: .touchUpInside
        )
        addRecipeBtn.clipsToBounds = true
        addRecipeBtn.translatesAutoresizingMaskIntoConstraints = false
        view.addSubview(addRecipeBtn)
        addRecipeBtn.bottomAnchor.constraint(
          equalTo: view.bottomAnchor,
          constant: -20
        ).isActive = true
        addRecipeBtn.trailingAnchor.constraint(
          equalTo: view.trailingAnchor,
          constant: -20
        ).isActive = true
        addRecipeBtn.heightAnchor.constraint(
          equalToConstant: addRecipeBtnWidth
        ).isActive = true
        addRecipeBtn.widthAnchor.constraint(
          equalToConstant: addRecipeBtnWidth
        ).isActive = true
    }

}
