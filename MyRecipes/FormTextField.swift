//
//  FormTextField.swift
//  MyRecipes
//
//  Created by Tobias Ostner on 4/7/17.
//  Copyright © 2017 Tobias Ostner. All rights reserved.
//

import UIKit

final class FormTextField: UITextField, UITextFieldDelegate {
    private(set) var value: String = ""
    private let callback: (String) -> ()

    init(frame: CGRect, callback: @escaping (String) -> ()) {
        self.callback = callback
        super.init(frame: frame)
        clearButtonMode = .whileEditing
        delegate = self
        autocorrectionType = .no
    }

    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    override func resignFirstResponder() -> Bool {
        super.resignFirstResponder()
        print("resign first responder")
        callback(value)
        return true
    }

    // MARK: UITextFieldDelegate

    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.endEditing(true)
        return false
    }

    func textField(
      _ textField: UITextField,
      shouldChangeCharactersIn nsrange: NSRange,
      replacementString string: String)
      -> Bool
    {
        guard let range = nsrange.toRange() else { return false }
        var chars = Array(value.characters)
        if string.isEmpty {
            chars.remove(at: range.lowerBound)
        } else {
            chars.insert(Character(string), at: range.lowerBound)
        }
        value = String(chars)
        return true
    }
}
