//
//  NewRecipeCell.swift
//  MyRecipes
//
//  Created by Tobias Ostner on 4/6/17.
//  Copyright © 2017 Tobias Ostner. All rights reserved.
//

import UIKit

protocol NewRecipeCellDelegate: class {
    func newRecipeCell(_ cell: NewRecipeCell, didCommitValue value: String)
    func didDeleteNewRecipeCell(_ cell: NewRecipeCell)
}

final class NewRecipeCell: UITableViewCell {

    weak var delegate: NewRecipeCellDelegate?

    lazy var deleteButton: UIButton = {
        let btn = UIButton(type: .system)
        btn.setTitle("X", for: .normal)
        btn.sizeToFit()
        btn.addTarget(
          self,
          action: #selector(self.deleteCell),
          for: .touchUpInside
        )
        return btn
    }()

    lazy var textField: FormTextField = {
        let tf = FormTextField(frame: .zero) {
            value in
            self.delegate?.newRecipeCell(self, didCommitValue: value)
        }
        tf.translatesAutoresizingMaskIntoConstraints = false
        return tf
    }()

    var enableDeletion: Bool = true {
        didSet {
            editingAccessoryView = enableDeletion ? deleteButton : nil
        }
    }

    override init(style: UITableViewCellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        contentView.addSubview(textField)
        textField.constraintToEdgesOfParent()
    }

    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    override func prepareForReuse() {
        super.prepareForReuse()
        textField.text = ""
        editingAccessoryView = nil
        delegate = nil
    }

    func deleteCell() {
        delegate?.didDeleteNewRecipeCell(self)
    }

}
