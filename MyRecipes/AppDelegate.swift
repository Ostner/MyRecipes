//
//  AppDelegate.swift
//  MyRecipes
//
//  Created by Tobias Ostner on 3/29/17.
//  Copyright © 2017 Tobias Ostner. All rights reserved.
//

import UIKit

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    let baseURL = URL(string: "http://localhost:8080")!

    var window: UIWindow?

    func application(_ application: UIApplication,
                     didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?)
      -> Bool
    {
        window = UIWindow()
        let recipesVC = RecipesViewController()
        let rootVC = UINavigationController(rootViewController: recipesVC)
        window?.rootViewController = rootVC
        window?.makeKeyAndVisible()
        return true
    }
}
