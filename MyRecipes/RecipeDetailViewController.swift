//
//  RecipeDetailViewController.swift
//  MyRecipes
//
//  Created by Tobias Ostner on 4/2/17.
//  Copyright © 2017 Tobias Ostner. All rights reserved.
//

import UIKit

final class RecipeDetailViewController: UIViewController {

    let recipe: Recipe

    let scrollView: UIScrollView = {
        let sv = UIScrollView()
        sv.translatesAutoresizingMaskIntoConstraints = false
        sv.contentInset = UIEdgeInsets(top: 30, left: 0, bottom: 0, right: 0)
        return sv
    }()

    let imageView: UIImageView = {
        let iv = UIImageView()
        iv.translatesAutoresizingMaskIntoConstraints = false
        return iv
    }()

    let nameLbl: UILabel = {
        let lbl = UILabel()
        lbl.translatesAutoresizingMaskIntoConstraints = false
        return lbl
    }()

    var contentView: UIStackView = {
        let sv = UIStackView()
        sv.axis = .vertical
        sv.alignment = .center
        sv.translatesAutoresizingMaskIntoConstraints = false
        return sv
    }()

    init(recipe: Recipe) {
        self.recipe = recipe
        super.init(nibName: nil, bundle: nil)
    }

    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    override func viewDidLoad() {
        super.viewDidLoad()

        view.addSubview(scrollView)
        scrollView.constraintToEdgesOfParent()
        scrollView.backgroundColor = .yellow

        nameLbl.translatesAutoresizingMaskIntoConstraints = false
        nameLbl.text = recipe.name
        imageView.translatesAutoresizingMaskIntoConstraints = false
        imageView.backgroundColor = .red

        contentView.addArrangedSubview(imageView)
        contentView.addArrangedSubview(nameLbl)
        scrollView.addSubview(contentView)
        contentView.translatesAutoresizingMaskIntoConstraints = false
        contentView.widthAnchor.constraint(
          equalTo: view.widthAnchor
        ).isActive = true
        contentView.constraintToEdgesOfParent()

        imageView.heightAnchor.constraint(equalToConstant: 200).isActive = true
        imageView.widthAnchor.constraint(equalToConstant: 200).isActive = true
    }
}
