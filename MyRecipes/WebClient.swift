//
//  WebClient.swift
//  MyRecipes
//
//  Created by Tobias Ostner on 4/8/17.
//  Copyright © 2017 Tobias Ostner. All rights reserved.
//

import Foundation

final class WebClient {

    let baseURL = URL(string: "http://localhost:8080")!

    func getRecipes(completion: @escaping ([Recipe]?) -> ()) {
        let url = URL(string: "recipes", relativeTo: baseURL)!
        URLSession.shared.dataTask(with: url) {
            data, _, _ in
            guard let data = data,
                  let json = try? JSONSerialization.jsonObject(with: data, options: []),
                  let jsonArray = json as? [[String:Any]]
            else { completion(nil); return }
            completion(jsonArray.flatMap(Recipe.init))
        }.resume()
    }

    func post(_ recipe: Recipe, completion: @escaping (Recipe?) -> ()) {
        let url = URL(string: "recipes", relativeTo: baseURL)!
        var request = URLRequest(url: url)
        request.httpMethod = "POST"
        request.httpBody = recipe.jsonData
        request.addValue("application/json", forHTTPHeaderField: "content-type")
        URLSession.shared.dataTask(with: request) {
            data, _, _ in
            guard let data = data,
                  let result = try? JSONSerialization.jsonObject(with: data, options: []),
                  let json = result as? [String:Any],
                  let id = json["id"] as? Int
            else { return }
            for ingredient in recipe.ingredients {
                self.post(ingredient, with: id)
            }
            for instruction in recipe.instructions {
                self.post(instruction, with: id)
            }
            completion(Recipe(json: json))
        }.resume()
    }

    func post(_ ingredient: Ingredient, with id: Int) {
        let url = URL(string: "recipes/\(id)/ingredients", relativeTo: baseURL)!
        var request = URLRequest(url: url)
        request.httpMethod = "POST"
        request.httpBody = ingredient.jsonData
        request.addValue("application/json", forHTTPHeaderField: "content-type")
        URLSession.shared.dataTask(with: request) {
            _, response, _ in
        }.resume()
    }

    func post(_ instruction: Instruction, with id: Int) {
        let url = URL(string: "recipes/\(id)/instructions", relativeTo: baseURL)!
        var request = URLRequest(url: url)
        request.httpMethod = "POST"
        request.httpBody = instruction.jsonData
        request.addValue("application/json", forHTTPHeaderField: "content-type")
        URLSession.shared.dataTask(with: request) {
            _, response, _ in
            if let response = response as? HTTPURLResponse {
                print("post ingredient, statuscode: \(response.statusCode)")
            } else {
                print("post ingredient, error no response")
            }
        }.resume()
    }

}
