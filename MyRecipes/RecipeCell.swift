//
//  RecipeCell.swift
//  MyRecipes
//
//  Created by Tobias Ostner on 4/2/17.
//  Copyright © 2017 Tobias Ostner. All rights reserved.
//

import UIKit

final class RecipeCell: UICollectionViewCell {

    let title: UILabel = {
        let lbl = UILabel()
        lbl.translatesAutoresizingMaskIntoConstraints = false
        return lbl
    }()

    override init(frame: CGRect) {
        super.init(frame: frame)
        contentView.addSubview(title)
        title.constraintToEdgesOfParent()
    }

    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    override func prepareForReuse() {
        title.text = nil
    }
}
