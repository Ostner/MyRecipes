import Vapor
import Fluent

final class Ingredient: Model {

    var id: Node?
    var exists = false
    var title: String

    var recipeId: Node?

    init(title: String, recipeId: Node?) {
        self.title = title
        self.recipeId = recipeId
    }

    init(node: Node, in context: Context) throws {
        id = try node.extract("id")
        title = try node.extract("title")
        recipeId = try node.extract("recipe_id")
    }

    func makeNode(context: Context) throws -> Node {
        return try Node(node: ["id": id, "title": title, "recipe_id": recipeId])
    }

    static func prepare(_ database: Database) throws {
        try database.create("ingredients") { ingredients in
            ingredients.id()
            ingredients.string("title")
            ingredients.parent(Recipe.self, optional: false)
        }
    }

    static func revert(_ database: Database) throws {
        try database.delete("ingredients")
    }

}
