import Vapor
import Fluent

final class Instruction: Model {

    var id: Node?
    var exists = false
    var step: Int
    var title: String

    var recipeId: Node?

    init(step: Int, title: String, recipeId: Node?) {
        self.step = step
        self.title = title
        self.recipeId = recipeId
    }

    init(node: Node, in context: Context) throws {
        id = try node.extract("id")
        step = try node.extract("step")
        title = try node.extract("title")
        recipeId = try node.extract("recipe_id")
    }

    func makeNode(context: Context) throws -> Node {
        return try Node(node: ["id": id,
                               "step": step,
                               "title": title,
                               "recipe_id": recipeId])
    }

    static func prepare(_ database: Database) throws {
        try database.create("instructions") { instructions in
            instructions.id()
            instructions.int("step")
            instructions.string("title")
            instructions.parent(Recipe.self, optional: false)
        }
    }

    static func revert(_ database: Database) throws {
        try database.delete("instructions")
    }
}
