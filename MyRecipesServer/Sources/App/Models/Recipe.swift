import Vapor
import Fluent
import Foundation

final class Recipe: Model {

    var id: Node?
    var exists = false
    var name: String

    init(name: String) {
        self.name = name
    }

    init(node: Node, in context: Context) throws {
        id = try node.extract("id")
        name = try node.extract("name")
    }

    func makeNode(context: Context) throws -> Node {
        return try Node(node: ["id": id, "name": name])
    }

    static func prepare(_ database: Database) throws {
        try database.create("recipes") { recipes in
            recipes.id()
            recipes.string("name")
        }
    }

    static func revert(_ database: Database) throws {
        try database.delete("recipes")
    }
}
