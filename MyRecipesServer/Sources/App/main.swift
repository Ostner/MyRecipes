import Vapor
import VaporSQLite

let drop = Droplet()
try drop.addProvider(VaporSQLite.Provider.self)
drop.preparations.append(Recipe.self)
drop.preparations.append(Ingredient.self)
drop.preparations.append(Instruction.self)

let recipesController = MyRecipesController()
drop.resource("recipes", recipesController)

let ingredientsController = IngredientsController()
drop.resource("ingredients", ingredientsController)
ingredientsController.addRoutes(drop: drop)

let instructionsController = InstructionsController()
drop.resource("instructions", instructionsController)
instructionsController.addRoutes(drop: drop)

drop.run()
