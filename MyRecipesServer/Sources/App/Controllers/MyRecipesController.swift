import Vapor
import HTTP

final class MyRecipesController: ResourceRepresentable {

    func makeResource() -> Resource<Recipe> {
        return Resource(
          index: index,
          store: create,
          show: show,
          modify: update,
          destroy: delete)
    }

    func index(_ request: Request) throws -> ResponseRepresentable {
        return try JSON(node: Recipe.all())
    }

    func create(_ request: Request) throws -> ResponseRepresentable {
        var recipe = try request.recipe()
        try recipe.save()
        return recipe
    }

    func show(_ request: Request, _ recipe: Recipe) throws -> ResponseRepresentable {
        return recipe
    }

    func update(_ request: Request, _ recipe: Recipe) throws -> ResponseRepresentable {
        let new = try request.recipe()
        var recipe = recipe
        recipe.name = new.name
        try recipe.save()
        return recipe
    }

    func delete(_ request: Request, _ recipe: Recipe) throws -> ResponseRepresentable {
        try recipe.delete()
        return JSON([:])
    }

}

extension Request {
    func recipe() throws -> Recipe {
        guard let json = json else { throw Abort.badRequest }
        return try Recipe(node: json)
    }
}
