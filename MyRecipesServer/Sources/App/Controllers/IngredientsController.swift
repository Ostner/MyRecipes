import Vapor
import HTTP

final class IngredientsController: ResourceRepresentable {

    func makeResource() -> Resource<Ingredient> {
        return Resource(
          index: index,
          store: create
        )
    }

    func addRoutes(drop: Droplet) {
        drop.get("recipes",
                 Recipe.self,
                 "ingredients",
                 handler: getRecipeIngredients)
        drop.post("recipes",
                  Recipe.self,
                  "ingredients",
                  handler: postRecipeIngredient)
    }

    func index(_ request: Request) throws -> ResponseRepresentable {
        return try JSON(node: Ingredient.all())
    }

    func create(_ request: Request) throws -> ResponseRepresentable {
        var ingredient = try request.ingredient()
        try ingredient.save()
        return ingredient
    }

    func getRecipeIngredients(
      _ request: Request,
      recipe: Recipe)
      throws -> ResponseRepresentable
    {
        let ingredients = try recipe.children(nil, Ingredient.self).all()
        return try JSON(node: ingredients)
    }

    func postRecipeIngredient(
      _ request: Request,
      recipe: Recipe)
      throws -> ResponseRepresentable
    {
        guard let title = request.json?["title"]?.string
        else { throw Abort.badRequest }
        var ingredient = Ingredient(title: title, recipeId: recipe.id)
        try ingredient.save()
        return try JSON(node: ingredient)
    }
}

extension Request {
    func ingredient() throws -> Ingredient {
        guard let json = json else { throw Abort.badRequest }
        return try Ingredient(node: json)
    }
}
