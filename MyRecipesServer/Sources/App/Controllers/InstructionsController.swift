import Vapor
import HTTP

final class InstructionsController: ResourceRepresentable {

    func makeResource() -> Resource<Instruction> {
        return Resource(
          index: index,
          store: create
        )
    }

    func addRoutes(drop: Droplet) {
        drop.get("recipes",
                 Recipe.self,
                 "instructions",
                 handler: getRecipeInstructions)
        drop.post("recipes",
                  Recipe.self,
                  "instructions",
                  handler: postRecipeInstruction)
    }

    func index(_ request: Request) throws -> ResponseRepresentable {
        return try JSON(node: Instruction.all())
    }

    func create(_ request: Request) throws -> ResponseRepresentable {
        var instruction = try request.instruction()
        try instruction.save()
        return instruction
    }

    func getRecipeInstructions(
      _ request: Request,
      recipe: Recipe)
      throws -> ResponseRepresentable
    {
        let instructions = try recipe.children(nil, Instruction.self).all()
        return try JSON(node: instructions)
    }

    func postRecipeInstruction(
      _ request: Request,
      recipe: Recipe)
      throws -> ResponseRepresentable
    {
        guard let step = request.json?["step"]?.int,
              let title = request.json?["title"]?.string
        else { throw Abort.badRequest }
        var instruction = Instruction(step: step,
                                      title: title,
                                      recipeId: recipe.id)
        try instruction.save()
        return try JSON(node: instruction)
    }
}

extension Request {
    func instruction() throws -> Instruction {
        guard let json = json else { throw Abort.badRequest }
        return try Instruction(node: json)
    }
}
