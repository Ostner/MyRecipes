#!/bin/bash

clear
echo "Add Tabule-Salat"
curl -H 'Content-Type: application/json' -X POST -d '{"name": "Tabule-Salat", "favorite": 0}' localhost:8080/recipes

echo "Add ingredient Salatgurke"
curl -H 'Content-Type: application/json' -X POST -d '{"item": "Salatgurke", "recipe_id": 1}' localhost:8080/ingredients
